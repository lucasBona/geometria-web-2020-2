package br.ucsal.test.ted;

import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CalculoHipotenusaSteps {
    private final String URL = "file:///Users/lucas/git/geometria-web-2020-2/geometria/src/main/resources/webapp/triangulo.html";

    private static WebDriver driver;

    WebElement tipoCalculo;

    WebElement cateto1;

    WebElement cateto2;

    WebElement botaoCalculo;

    @BeforeStory
    public void prepare() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver");
        driver = new ChromeDriver();
    }

    @Given("estou na funcionalidade de calculo de triangulos")
    public void givenEstouNaFuncionalidadeDeCalculoDeTriangulos() {
        driver.get(URL);
    }

    @When("seleciono o tipo de calculo hipotenusa")
    public void whenSelecionoOTipoDeCalculo(String calculo) {
        tipoCalculo = driver.findElement(By.id("tipoCalculoSelect"));
        tipoCalculo.sendKeys(calculo);
    }

    @When("informo 3 para cateto1")
    public void whenInformoNumeroParaCateto1(String numero) {
        cateto1 = driver.findElement(By.id("cateto1"));
        cateto1.sendKeys(numero);
    }

    @When("informo 4 para cateto2")
    public void whenInformoNumeroParaCateto2(String numero) {
        cateto2 = driver.findElement(By.id("cateto2"));
        cateto2.sendKeys(numero);
    }

    @When("solicito que o calculo seja realizado")
    public void whenSolicitoQueOCalculoSejaRealizado() {
        botaoCalculo = driver.findElement(By.id("calcularBtn"));
        botaoCalculo.click();
    }

    @Then("a hipotenusa calculada sera 5")
    public void thenAHipotenusaCalculadaSeraResultado(String numero) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String resultado = js.executeScript("return document.getElementById('hipotenusa').value").toString();
        Assert.assertEquals(Integer.toString(5), resultado);
    }
}
